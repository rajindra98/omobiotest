import React from 'react';
import { toast } from 'react-toastify';
class LoginForm extends React.Component {

    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            username:props?.selectedItem?.username,
            password:props?.selectedItem?.password,
        }
        
    }

    submitForm(){
        fetch("", {
            method: 'post', 
            headers: new Headers({
                'Content-Type': 'application/json',
                Authorization:""
            }),
            body: JSON.stringify(this.state)
        })
        .then(res => {
            console.log(res?.status, 'result');
            if(res?.status == 201 ){
                toast("Loging Succesfully");
            }else{
                toast("Something Went Wrong");
            }
        }).catch(function() {
            console.log("error");
        });
    }
render() {
    return (
        <div className="container-fluid main-content">
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-sm-4 col-xs-12" >

                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-12">
                        <form className="form">
                            <div class="card-header-login">
                                <h3>Log In to Your Account</h3>
                            </div>
                            <div class="card-login">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="Email" placeholder="Email" />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" placeholder="Password" />
                                </div>
                                <div class="form-checker">
                                    <input type="checkbox" class="form-checkerUp" id="checkbox" />
                                    <label class="form-check-label" for="CheckBox">Keep me logged in</label>
                                </div>
                                <button onClick={()=>{ this.LoginForm() }} type="button" class="btn btn-primary">Loging</button>
                                <p>Forgot password ! </p>
                                <p>Need an account ? Sign up </p>
                            </div>
                        </form>
                    </div>
                    <div className="col-md-4 col-sm-4 col-xs-12">

                    </div>
                </div>
            </div>
        </div>
    );
}
}
export default LoginForm;