import './App.css';
import LoginForm from './components/view/logingForm';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <LoginForm/>
      </header>
    </div>
  );
}

export default App;
